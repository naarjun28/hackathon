import os
import tarfile
import time
import shutil
import re
import requests
import json

SLACK_URL = "https://app.slack.com/client/T019E9M05RU/D019PQJ4QBV"

def backup_scr():
    root = "c:\\"
    source = os.path.join(root,"Backup_source","sample.txt")
    destination = os.path.join(root,"Backup_destination")

    try:
        shutil.copy(source,destination)
        return True
    except:
        return False
    
def msg_to_slack(text):
    return requests.post(SLACK_URL, json.dumps({'text': text}))

if __name__ == '__main__':
    if(backup_scr == True):
        msg_to_slack("The backup is successful with 1 file...")

# for filename in os.listdir(dir_src):
#            shutil.copy( dir_src + filename, dir_dst)
