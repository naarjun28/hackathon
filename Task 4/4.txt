4. SP500 stocks information
a. Produce a report as an excel spreadsheet or csv showing some information about
the stocks in the SP500, you can download stock price data from Yahoo Finance,
Quandl or use pandas datareader to get live data. There is also historical SP500 data
available, though it would be better to source new data:
http://neueda.conygre.com/pydata/SP500.csv and
neueda.conygre.com/pydata/StockPrices.xlsx
b. Some examples of what you might investigate are:
i. Most and Least volatility
ii. Best 5 and Worst 5 (e.g. you could calculate returns)
iii. Most Actively Traded, Least Actively Traded – by using volume
iv. You could make the reports by day, by week, by month
c. There are many python packages for this one, e.g. pandas_datareader,
BeautifulSoup and requests to extract data from the internet. pandas, numpy, re to
query and analyse the data.
d. Creating some charts to illustrate and trends in the data should be a big part of this
task. There are many plotting and charting packages – e.g. seab