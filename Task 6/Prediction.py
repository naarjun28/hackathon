from flask import Flask, jsonify, Response
from flask_restplus import Api, Resource
from flask import render_template_string
import joblib
import logging
import pandas as pd
import pandas_datareader as pdr
from datetime import datetime

app = Flask(__name__)
api = Api(app, version='1.0', title='Bollinger Analysis',
          description="Bollinger Analysis for stock of apple")
ns = api.namespace('finance_data')
model = joblib.load("exp_salary.joblib")
log = logging.getLogger(__name__)
# logging.basicConfig(level=logging.WARNING, filename='warning_data.log')
logging.basicConfig(filename='finance_data.log')


# logging.basicConfig(level=logging.ERROR, filename='error_data.log')


@ns.route('/advice/<string:ticker>')
@ns.param('ticker', 'Company code in stock market')
class Finance(Resource):
    def get(self, ticker):
        end = datetime.today()
        start = datetime(end.year, end.month - 2, end.day + 20)
        try:
            df = pdr.get_data_yahoo(ticker, start, end)
        except Exception as ex:
            return Response("error:" + str(ex), status=404)

        df['MA20'] = df['Adj Close'].rolling(window=21).mean()
        df['20dSTD'] = df['Adj Close'].rolling(window=21).std()
        df['Upper'] = df['MA20'] + (df['20dSTD'] * 2)
        df['Lower'] = df['MA20'] - (df['20dSTD'] * 2)

        todays_price = df['Adj Close'][datetime.today().date()]
        upper_price = df['Upper'][datetime.today().date()]
        lower_price = df['Lower'][datetime.today().date()]
        if todays_price >= upper_price:
            return {
                'ticker': ticker,
                'Decision': 'SELL'
            }
        elif todays_price <= lower_price:
            return {
                'ticker': ticker,
                'Decision': 'BUY'
            }
        else:
            return {
                'ticker': ticker,
                'Decision': 'HOLD'
            }


if __name__ == '__main__':
    app.run(debug=True, port=7400)
